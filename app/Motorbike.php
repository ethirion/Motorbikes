<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motorbike extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motorbikes';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public $sortable = [ 'price' , 'created_at'];

}
