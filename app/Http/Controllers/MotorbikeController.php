<?php

namespace App\Http\Controllers;

use App\Motorbike;
use Faker\Provider\Image;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class MotorbikeController extends Controller
{

    /**
     * Register The motorbikes by admin.
     *
     * @return Response
     */
    public function register()
    {
        return view('registerMotorbikes');
    }


    /**
     * Create a new flight instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Validate the request...

        $motorbikes = new Motorbike;

        $motorbikes->name = $request->name;

        $motorbikes->color = $request->color;

        $motorbikes->price = $request->price;

        $motorbikes->weight = $request->weight;

        if( $request->hasFile('image')) {
            $image = $request->file('image');
            $path = public_path(). '/images/';
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->move($path, $filename);

            $motorbikes->image = $filename;
        }


        $motorbikes->save();

        return redirect()->route('show');
    }


    public function show()
    {
        $x = 0;

            $motorbikes = Motorbike::paginate(5);

        return view('show', compact('motorbikes','x'));
    }


    public function showmotor($id)
    {
        $motorbike = Motorbike::find($id);

        return view('showmotor', compact('motorbike'));
    }

    public function showbyfilter(Request $request)
    {

        $x = 0;

        $motorbikes = Motorbike::where('color',$request->color)->paginate(5);

        return view('show', compact('motorbikes','x'));

    }
    public function orderingShow(Request $request)
    {



        $x = $request->orderBy;

        if ($x == 2)
        {
            $motorbikes = Motorbike::orderBy('price','desc')->paginate(5);
        }
        elseif ($x == 1)
        {
            $motorbikes = Motorbike::orderBy('created_at','desc')->paginate(5);
        }
        else{
            $x = 0;

            $motorbikes = Motorbike::paginate(5);
        }

        return view('show', compact('motorbikes','x'));
    }
}
