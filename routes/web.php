<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('registermotorbikes', 'MotorbikeController@register')->middleware(['auth','admin']);

Route::post('savemotorbikes' , 'MotorbikeController@store')->name('store')->middleware(['auth','admin']);

Route::get('showmotorbikes' , 'MotorbikeController@show')->name('show');

Route::get('showmotorsbyfilter' , 'MotorbikeController@showbyfilter')->name('showFilter');

Route::get('showmotor/{id}', 'MotorbikeController@showmotor')->name('showmotor');

Route::get('orderingshow','MotorbikeController@orderingShow')->name('ordershow');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
