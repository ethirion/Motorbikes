<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Styles -->
    <style>

        body{padding-top:30px;}

        .glyphicon {  margin-bottom: 10px;margin-right: 10px;}

        small {
            display: block;
            line-height: 1.428571429;
            color: #999;
        }

    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="container">
        <div class="form-group">
            <form method="GET" action="{{ route('showFilter') }}">
                <input type="text" placeholder="filter by color input color" name="color" id="search" class="">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input value="Filter" type="submit">
            </form>
            <form method="GET" action="{{ route('ordershow') }}">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="orderBy" value="1">
                <input value="SortbyDate" type="submit">
            </form>
            <form method="GET" action="{{ route('ordershow') }}">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="orderBy" value="2">
                <input value="SortbyPrice" type="submit">
            </form>
        </div>
        <div class="row">
            @foreach($motorbikes as $motorbike)
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <img src="{{asset('images/' . $motorbike->image)}}" alt="{{ $motorbike->name }}" class="img-rounded img-responsive" />
                        </div>
                        <div class="col-sm-6 col-md-8">
                            <h4>{{ $motorbike->name }}</h4>
                            <p>
                                <i class="glyphicon glyphicon-envelope"></i>{{ $motorbike->color }}
                                <br />
                                <i class="glyphicon glyphicon-globe"></i>{{ $motorbike->weight }}Kg
                                <br />
                                <i class="glyphicon glyphicon-gift"></i>{{ $motorbike->price }}T</p>
                            <!-- Split button -->
                            <form method="GET" action="{{ route('showmotor', $motorbike->id) }}">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-primary">
                                        Continue</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @if($x == 2)
            @if ($motorbikes->lastPage() > 1)
                <ul class="pagination">
                    <li class="{{ ($motorbikes->currentPage() == 1) ? ' disabled' : '' }}">
                        <a href="{{ $motorbikes->url(1) }}">Previous</a>
                    </li>
                    @for ($i = 1; $i <= $motorbikes->lastPage(); $i++)
                        <li class="{{ ($motorbikes->currentPage() == $i) ? ' active' : '' }}">
                            <a href="{{ $motorbikes->url($i) }}">{{ $i }}</a>
                        </li>
                    @endfor
                    <li class="{{ ($motorbikes->currentPage() == $motorbikes->lastPage()) ? ' disabled' : '' }}">
                        <a href="{{ $motorbikes->url($motorbikes->currentPage()+1) }}" >Next</a>
                    </li>
                </ul>
            @endif

            @elseif($x == 1)

            {{ $motorbikes->links() }}

        @elseif($x == 0)
        <div class="row">
            {{ $motorbikes->links() }}
        </div>
            @endif
    </div>
</div>
</body>
</html>

